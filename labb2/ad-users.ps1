﻿
# Enkel lösning ifrån föreläsningen (copy-paste och justera)
New-AdUser –Name "user1" -GivenName "User" -SurName "1" -Path "OU=JKPG,OU=Sales,OU=Domain Users,OU=ljvi,DC=ljvi1798,DC=dnlab,DC=local" -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)


# Enkel lösning ifrån föreläsningen, bättre formatterad (copy-paste och justera)
New-AdUser –Name "user1" `
           -GivenName "User" `
           -SurName "1" `
           -Path "OU=JKPG,OU=Sales,OU=Domain Users,OU=ljvi,DC=ljvi1798,DC=dnlab,DC=local" `
           -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)


# Mer komplex lösning (logisk struktur)
$Domain = "DC=ljvi1798,DC=dnlab,DC=local"
$JKPGPath = "OU=JKPG,OU=Domain Users,OU=ljvi"
$GBGPath = "OU=GBG,OU=Domain Users,OU=ljvi"

$OUs = @("Accounting", "IT", "Sales", "Purchase")

foreach ($OU in $OUs) {
    foreach ($i in 1..5) {
        New-AdUser –Name ("$OU" + "User0" + "$i") `
                   -GivenName "$OU" `
                   -SurName "User0$i" `
                   -Path "OU=$OU,$JKPGPath,$Domain" `
                   -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)

        New-AdUser –Name ("$OU" + "User0" + "$i") `
                   -GivenName "$OU" `
                   -SurName "User0$i" `
                   -Path "OU=$OU,$GBGPath,$Domain" `
                   -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)
    }
}


# Mer komplex lösning (locations struktur)
$Domain = "DC=ljvi1798,DC=dnlab,DC=local"
$JKPGPath = "OU=USERS,OU=JKPG,OU=SE,OU=ljvi"
$GBGPath = "OU=USERS,OU=GBG,OU=SE,OU=ljvi"
$CPHGPath = "OU=USERS,OU=CPHG,OU=SE,OU=ljvi"

$OUs = @("Accounting", "IT", "Sales", "Purchase")

foreach ($OU in $OUs) {
    foreach ($i in 1..5) {
        New-AdUser –Name ("$OU" + "User0" + "$i") `
                   -GivenName "$OU" `
                   -SurName "User0$i" `
                   -Path "OU=$OU,$JKPGPath,$Domain" `
                   -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)

        New-AdUser –Name ("$OU" + "User0" + "$i") `
                   -GivenName "$OU" `
                   -SurName "User0$i" `
                   -Path "OU=$OU,$GBGPath,$Domain" `
                   -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)
                   
        New-AdUser –Name ("$OU" + "User0" + "$i") `
                   -GivenName "$OU" `
                   -SurName "User0$i" `
                   -Path "OU=$OU,$CPHGPath,$Domain" `
                   -AccountPassword (ConvertTo-SecureString "Pa$$w0rd" –AsPlainText $true)
    }
}
